package ASS.assignment1.q14;

import java.util.Date;

public class SwitchCase {

	public static void main(String[] args) {
		
		
		Date d = new Date();
		int i = 1;
		int num = 9;
		switch(i) {
		case 1:
			System.out.println(Math.sqrt(num));
			break;
		case 2:
			System.out.println(d);
			break;
		case 3:
			String s = "I am learning core java";
			System.out.println(s);
			
		}

	}

}
